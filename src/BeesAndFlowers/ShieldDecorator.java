/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 4
 * Author:     Molly Uchtman
 * Date:       1/19/19
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.scene.control.Label;

/**
 * Protects the bee from harm
 */
public class ShieldDecorator extends BeeDecorator {

    private Bee bee;
    private int beeEnergy;
    private boolean applied;

    public ShieldDecorator(Bee bee){
        super(bee);
        System.out.println("shield created");
        this.bee = bee;
        applied = true;
    }

    @Override
    public boolean move() {
        //System.out.println("shield move");
        boolean destination = bee.move();
        if(applied){
            beeEnergy -= 1;
            if(beeEnergy > bee.getEnergy()){
                int energy = beeEnergy - bee.getEnergy();
                bee.changeEnergy(energy);
                applied = false;
            } else if(beeEnergy < bee.getEnergy()){
                beeEnergy = bee.getEnergy();
            }
        }
        return destination;
    }

    public Label getEnergyLabel() {
        Label label;
        if(applied){
            label = new Label(bee.getEnergyLabel().getText() + " #");
        } else {
            label = bee.getEnergyLabel();
        }
        return label;
    }

    public void printType(){
        bee.printType();
        System.out.println("shield");
    }

}

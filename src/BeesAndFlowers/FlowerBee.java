/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Molly Uchtman
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;

/**
 * a type of bee that move to the nearest flower
 */
public class FlowerBee extends Bee {
    static final String TEXTURE_PATH = FlowerBee.class.getClassLoader().getResource("bee-1.png").toString();

    private Point2D destination;

    public FlowerBee(int energy, Point2D position, Point2D destination) {
        super(energy, position, TEXTURE_PATH);
        this.destination = destination;
        speed = 3;
    }

    @Override
    public boolean move() {
        int xSpeed = findXSpeed();
        int ySpeed = findYSpeed();
        changePosition(xSpeed, ySpeed);
        return checkCollision(destination, 10);
    }

    public void setDestination(Point2D destination) {
        this.destination = destination;
    }

    private int findXSpeed() {
        int xSpeed;
        if (destination.getX() > getXPosition()) {
            xSpeed = speed;
        } else if (destination.getX() < getXPosition()) {
            xSpeed = -1 * speed;
        } else {
            xSpeed = 0;
        }
        return xSpeed;
    }

    private int findYSpeed() {
        int ySpeed;
        if (destination.getY() > getYPosition()) {
            ySpeed = speed;
        } else if (destination.getY() < getYPosition()) {
            ySpeed = -1 * speed;
        } else {
            ySpeed = 0;
        }
        return ySpeed;
    }

    public void printType(){
        System.out.println("flower");
    }


}

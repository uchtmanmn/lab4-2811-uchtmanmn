/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Molly Uchtman and Dalton Smith
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * controls everything that happens in the garden
 */
public class GardenController implements Initializable {
    private final int TEXTURE_SIZE = 50;
    private final Random random = new Random();
    private List<BeeContainer> bees;
    private List<Flower> flowers;

    @FXML
    private Pane gardenPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gardenPane.setBackground(new Background(new BackgroundImage(
                new Image(getClass().getClassLoader().getResource("garden.jpg").toString()),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT)));

        flowers = generateFlowers(random.nextInt(40) + 10);
        bees = generateBees(random.nextInt(40) + 10);

        for (Flower flower : flowers) {
            gardenPane.getChildren().add(flower.getTexture());
        }
        for (BeeContainer bee : bees) {
            gardenPane.getChildren().add(bee.getBee().getTexture());
            gardenPane.getChildren().add(bee.getBee().getEnergyLabel());
        }
    }

    public List<Flower> generateFlowers(int numFlowers) {
        List<Flower> flowerList = new ArrayList<>();
        for(int i = 0; i < numFlowers; i++) {
            double chooser = random.nextDouble();
            if(chooser < 0.33) {
                flowerList.add(new GoodFlower(generateRandomPoint(), 10));
            } else if(chooser < 0.66){
                flowerList.add(new BadFlower(generateRandomPoint(), 5));
            } else {
                flowerList.add(new PowerUpFlower(generateRandomPoint(), 0));
            }
        }

        return flowerList;
    }

    public List<BeeContainer> generateBees(int numBees) {
        List<BeeContainer> beeList = new ArrayList<>();
        for(int i = 0; i < numBees; i++) {
            if(random.nextDouble() > 0.5) {
                beeList.add(new BeeContainer(new FlowerBee(200, generateRandomPoint(), flowers.get(random.nextInt(flowers.size())).getPosition())));
            } else {
                beeList.add(new BeeContainer(new VerticalBee(200, generateRandomPoint(), (int)gardenPane.getPrefHeight() - TEXTURE_SIZE)));
            }
        }

        return beeList;
    }

    public Point2D generateRandomPoint() {
        return new Point2D(random.nextInt((int)gardenPane.getPrefWidth()) - TEXTURE_SIZE, random.nextInt((int)gardenPane.getPrefHeight()) - TEXTURE_SIZE);
    }

    public void nextTick() {
        ArrayList<Bee> deadBees = new ArrayList<>();

        for(BeeContainer bee : bees) {
            for(Flower flower : flowers) {
                if(bee.getBee().checkCollision(flower.getPosition(), 20)) {
                    bee.getBee().changeEnergy(flower.landedOn());
                    bee.setBee(flower.powerUpBee(bee.getBee()));

                }
            }
            if(bee.move()) {
                bee.getBee().setDestination(flowers.get(random.nextInt(flowers.size())).getPosition());
            }
            bee.getBee().changeEnergy(-1);
            if (bee.getBee().checkDead()) {
                gardenPane.getChildren().remove(bee.getBee().getTexture());
                gardenPane.getChildren().remove(bee.getBee().getEnergyLabel());
                deadBees.add(bee.getBee());
            }
        }
        for (Bee deadBee : deadBees) {
            bees.remove(deadBee);
        }
    }

    @FXML
    private void onKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.UP) {
            nextTick();
        }
    }

}
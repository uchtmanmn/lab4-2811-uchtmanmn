package BeesAndFlowers;

public class BeeContainer {
    private Bee bee;

    public BeeContainer(Bee bee){
        this.bee = bee;
    }

    public Bee getBee(){
        return bee;
    }

    public boolean move(){
        return bee.move();
    }

    public void setBee(Bee bee){
        this.bee = bee;
    }
}

/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 4
 * Author:     Molly Uchtman
 * Date:       1/19/19
 */
package BeesAndFlowers;

import javafx.scene.control.Label;

import java.awt.*;

/**
 * changes the amount the bee moves randomly for 10 ticks
 */
public class MovementDecorator extends BeeDecorator {
    private Bee bee;
    private int ticks;

    public MovementDecorator(Bee bee) {
        super(bee);
        System.out.println("movement created");
        this.bee = bee;
        ticks = 15;
    }

    @Override
    public boolean move() {
        boolean destination = false;
        if (ticks == 0) {
            destination = bee.move();
        } else {
            for (int i = 0; i < Math.random() * 4; i++) {
                if (bee.move()) {
                    destination = true;
                }
                this.changePosition(bee.getXPosition(), bee.getYPosition());
            }
            ticks -= 1;
            System.out.println(ticks);
        }
        return destination;
    }

    public javafx.scene.control.Label getEnergyLabel() {
        javafx.scene.control.Label label;
        if(ticks > 0){
            label = new Label(bee.getEnergyLabel().getText() + " !");
        } else {
            label = bee.getEnergyLabel();
        }
        return label;
    }

    public void printType(){
        bee.printType();
        System.out.println("movement");
    }
}

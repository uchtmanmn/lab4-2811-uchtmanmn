/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 4
 * Author:     Molly Uchtman
 * Date:       1/19/19
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * adds a buff to the bee that changes its behavior
 */
public abstract class BeeDecorator extends Bee{
    private Bee wrappedBee;
    private String symbol;

    public enum Decorations{
        SHIELD, SPEED, MOVEMENT;
    }

    public BeeDecorator(Bee wrappedBee) {
        super(wrappedBee.getEnergy(), new Point2D(wrappedBee.getXPosition(), wrappedBee.getYPosition()), wrappedBee.getPath());
        this.wrappedBee = wrappedBee;
    }

    @Override
    public void setDestination(Point2D destination) {
        wrappedBee.setDestination(destination);
    }

    public void changeEnergy(int amount) {
        wrappedBee.changeEnergy(amount);

    }

    @Override
    public abstract boolean move();

    public Label getEnergyLabel() {
        return wrappedBee.getEnergyLabel();
    }

    public void printType(){
        System.out.println("decorator");
    }

    public boolean checkDead() {
        return wrappedBee.checkDead();
    }

    public boolean checkCollision(Point2D object, int tolerance) {
        return wrappedBee.checkCollision(object, tolerance);
    }

    public double getXPosition() {
        return wrappedBee.getXPosition();
    }

    public double getYPosition() {
        return wrappedBee.getYPosition();
    }

    public ImageView getTexture() {
        return wrappedBee.getTexture();
    }

}

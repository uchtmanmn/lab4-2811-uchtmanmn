/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Molly Uchtman
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

/**
 * an object that represents a bee. It has energy, x and y position in the garden, and can 'move'
 * When out of energy the bee is considered dead
 */
public abstract class Bee{
    private Point2D position;
    private int energy;
    private ImageView texture;
    private Label energyLabel;
    protected int speed;
    private String path;

    public Bee(int energy, Point2D position, String path) {
        this.energy = energy;
        this.position = position;
        this.path = path;
        this.texture = new ImageView(new Image(path));
        texture.setFitHeight(50D);
        texture.setFitWidth(50D);
        texture.setX(position.getX());
        texture.setY(position.getY());
        energyLabel = new Label();
        energyLabel.setTextFill(Color.WHITE);
        energyLabel.setText(String.valueOf(energy));
        energyLabel.setLayoutX(position.getX());
        energyLabel.setLayoutY(position.getY() - 10);
    }

    public String getPath(){
        return path;
    }

    public boolean checkDead() {
        return energy == 0;
    }

    public int getEnergy() {
        return energy;
    }

    /**
     * changes energy by amount. Use negative number to decrease energy
     *
     * @param amount amount to change energy by
     */
    public void changeEnergy(int amount) {
        energy += amount;
        energyLabel.setText(String.valueOf(energy));

    }

    /**
     * change position by x and y
     *
     * @param x horizontal location change
     * @param y vertical location change
     */
    public void changePosition(double x, double y) {
        position = position.add(x, y);
        texture.setX(position.getX());
        texture.setY(position.getY());
        energyLabel.setLayoutX(position.getX());
        energyLabel.setLayoutY(position.getY() - 10);
    }

    public void collided() {
        energy--;
    }

    public Point2D getPosition() {
        return position;
    }

    public double getXPosition() {
        return position.getX();
    }

    public double getYPosition() {
        return position.getY();
    }


    /**
     * checks if the bee has collided with an object within a tolerance
     *
     * @param object    location of the object
     * @param tolerance how close the bee and object must bee to have collided
     * @return if the bee has collided
     */
    public boolean checkCollision(Point2D object, int tolerance) {
        return position.distance(object) < tolerance;
    }

    public abstract boolean move();

    public abstract void setDestination(Point2D destination);

    public double getSpeed(){
        return speed;
    }

    public void setSpeed(){
        this.speed = speed;
    }

    public ImageView getTexture() {
        return texture;
    }

    public Label getEnergyLabel() {
        return energyLabel;
    }

    public void printType(){
        System.out.println("bee");
    }
}

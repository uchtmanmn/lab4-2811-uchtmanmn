/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Molly Uchtman
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;

/**
 * A type of bee that only changes its Y position when moving
 */
public class VerticalBee extends Bee {
    static final String TEXTURE_PATH = VerticalBee.class.getClassLoader().getResource("bee-2.png").toString();

    private int screenHeight;

    public VerticalBee(int energy, Point2D position, int screenHeight) {
        super(energy, position, TEXTURE_PATH);
        this.screenHeight = screenHeight;
        speed = 5;
    }

    /**
     * moves the bee
     *
     * @return true if new destination must be set
     */
    @Override
    public boolean move() {
        changePosition(0, speed);
        if (getYPosition() <= 0) {
            speed = 5;
        } else if(getYPosition() >= screenHeight) {
            speed = -5;
        }
        return false;
    }

    @Override
    public void setDestination(Point2D destination) {
        //no destination to set
    }

    public void printType(){
        System.out.println("vertical");
    }
}

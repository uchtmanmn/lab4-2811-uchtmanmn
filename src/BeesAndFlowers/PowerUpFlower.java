package BeesAndFlowers;

import javafx.geometry.Point2D;

public class PowerUpFlower extends Flower {
    private boolean landedOn;
    private static final String TEXTURE_PATH = PowerUpFlower.class.getClassLoader().getResource("violet.jpg").toString();

    public PowerUpFlower(Point2D position, int energyProvided) {
        super(position, energyProvided, TEXTURE_PATH);
        landedOn = false;
    }

    @Override
    public int landedOn() {
        return 0;
    }

    @Override
    public Bee powerUpBee(Bee bee){
        //System.out.println("power up bee");
        Bee newBee = bee;
        if(!landedOn){
            int decoratorChooser = (int) (Math.random()*3);
            if(decoratorChooser == 0){
                newBee = new ShieldDecorator(bee);
          //      System.out.println("Shield added");
            } else if(decoratorChooser == 2){
                newBee = new SpeedDecorator(bee);
          //      System.out.println("speed added");
            } else {
                newBee = new MovementDecorator(bee);
          //      System.out.println("move added");
            }
        }
        landedOn = true;
        return newBee;
    }
}

/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Dalton Smith
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.scene.image.ImageView;

/**
 * a type of flower that takes bees energy
 * the amount of energy taken halves each time it is 'landed on' until the flower will no longer provide energy
 */
public class BadFlower extends Flower {
    private static final String TEXTURE_PATH = BadFlower.class.getClassLoader().getResource("nightshade.png").toString();

    public BadFlower(Point2D position, int energyRemoved) {
        super(position, energyRemoved, TEXTURE_PATH);
    }

    @Override
    public int landedOn() {
        int energy = super.getEnergyProvided();
        super.setEnergyProvided(super.getEnergyProvided() / 2);
        return -1 * energy;
    }
}

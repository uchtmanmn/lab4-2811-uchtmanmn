/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 4
 * Author:     Molly Uchtman
 * Date:       1/19/19
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.scene.control.Label;

import java.awt.*;

/**
 * doubles the speed of the bee
 */
public class SpeedDecorator extends BeeDecorator {
    private Bee bee;
    private int ticksToLive;

    public SpeedDecorator(Bee bee) {
        super(bee);
        System.out.println("speed created");
        this.bee = bee;
    }

    @Override
    public boolean move() {
        boolean destination = false;
        if(ticksToLive == 0){
            destination = bee.move();
        } else {
            if(bee.move()){
                destination = true;
            }
            if(bee.move()){
                destination = true;
            }
            ticksToLive -= 1;
            this.changePosition(bee.getXPosition(), bee.getYPosition());
            System.out.println(ticksToLive);
        }
        return destination;
    }

    public javafx.scene.control.Label getEnergyLabel() {
        javafx.scene.control.Label label;
        if(ticksToLive > 0){
            label = new Label(bee.getEnergyLabel().getText() + " ?");
        } else {
            label = bee.getEnergyLabel();
        }
        return label;
    }

    public void printType(){
        bee.printType();
        System.out.println("speed");
    }

}

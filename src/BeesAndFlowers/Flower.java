/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Dalton Smith
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.concurrent.ThreadLocalRandom;

/**
 * an object representing a flower. It has energy and an image
 */
public abstract class Flower {
    private int energyProvided;
    private Point2D position;
    private ImageView texture;
    private BeeDecorator decorator;

    public Flower(Point2D position, int energyProvided, String path) {
        this.position = position;
        this.energyProvided = energyProvided;
        this.texture = new ImageView(new Image(path));
        texture.setFitHeight(50D);
        texture.setFitWidth(50D);
        texture.setX(position.getX());
        texture.setY(position.getY());
    }

    public int getEnergyProvided() {
        return energyProvided;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setEnergyProvided(int energyProvided) {
        this.energyProvided = energyProvided;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    public ImageView getTexture() {
        return texture;
    }

    public Bee powerUpBee(Bee bee){
        return bee;
    }

    public abstract int landedOn();
}

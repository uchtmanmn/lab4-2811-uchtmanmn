/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2b
 * Author:     Dalton Smith
 * Date:       12/19/18
 */
package BeesAndFlowers;

import javafx.geometry.Point2D;

/**
 * a type of flower that provides bees with energy
 * the amount of energy provided halves each time it is 'landed on' until the flower will no longer provide energy
 */
public class GoodFlower extends Flower {
    private static final String TEXTURE_PATH = GoodFlower.class.getClassLoader().getResource("aster.png").toString();

    public GoodFlower(Point2D position, int energyProvided) {
        super(position, energyProvided, TEXTURE_PATH);
    }

    @Override
    public int landedOn() {
        int energy = super.getEnergyProvided();
        super.setEnergyProvided(super.getEnergyProvided() / 2);
        return energy;
    }
}
